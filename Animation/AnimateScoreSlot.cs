﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnimateScoreSlot : MonoBehaviour {

	private Text T;
	private Animator Anim;
	private int OldT;
	private float time;
	private RectTransform transform;

	public bool HighScore;
	public float WaveAmplitude;
	public float WavePhase;
	public float WaveSpeed;

	// Use this for initialization
	void Start () {

		T = GetComponent<Text>();
		Anim = GetComponent<Animator>();

		transform = GetComponent<RectTransform> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (HighScore) {

			if (Anim.enabled) {

				Anim.enabled = false;

			}

			time += Time.deltaTime * WaveSpeed;

			Vector3 ancPos = transform.anchoredPosition;
			ancPos.y = Mathf.Sin (time + (transform.GetSiblingIndex () * WavePhase)) * WaveAmplitude;
			transform.anchoredPosition = ancPos;

		} else if (!Anim.enabled) {

			Anim.enabled = true;

		}

		if (int.Parse(T.text) > OldT) {

			Anim.SetTrigger("BumpUp");

		}

		if (int.Parse(T.text) < OldT) {

			Anim.SetTrigger("BumpDown");

		}

		OldT = int.Parse(T.text);
	
	}
}
