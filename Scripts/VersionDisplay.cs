﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VersionDisplay : MonoBehaviour {

	public string Prefix;
	public string Suffix;
	public bool useUnityVersion;

	private UnityEngine.UI.Text Text;

	void Start ()
	{

		Initialize ();

	}

	// Update is called once per frame
	void Update ()
	{

		if (Application.isPlaying) return;

		Initialize ();

	}

	void Initialize ()
	{

		if (!Text) {
			Text = GetComponent<UnityEngine.UI.Text> ();
		}

		if (!useUnityVersion)
			Text.text = Prefix + Application.version + Suffix;
		else
			Text.text = Prefix + Application.unityVersion + Suffix;

	}

}