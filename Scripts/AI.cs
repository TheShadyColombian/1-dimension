﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour {

	private float Lifetime;
	public float SpiralMulti;
	public Transform PlayerTrans;

	// Use this for initialization
	void Start () {

		PlayerTrans = GameObject.FindGameObjectWithTag("Player").transform;
	
	}
	
	// Update is called once per frame
	void Update () {

		Lifetime += Time.deltaTime;

		transform.position = new Vector3 (Scale(Mathf.Cos(Lifetime)), 
										  Scale(Mathf.Sin(Lifetime)));

		transform.LookAt(PlayerTrans);
	
	}

	private float Scale(float v) {
		return v * Mathf.Clamp(SpiralMulti - Lifetime, 0, Mathf.Infinity);
	}
}
	