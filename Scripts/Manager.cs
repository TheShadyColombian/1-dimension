﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class Manager : MonoBehaviour {

	public DotScheme [] Schemes;
	public DotScheme ActiveScheme;

	public GameObject AdMobManager;
	public GameObject GPGManager;
	public bool Paused;
	public int Score;
	public int HighScore;
	public string DisplayScore;

	public char[] Digits = new char[4];

	private Difficulty Diff;
	private Text[] ScoreDisplaySlots;
	private Text[] CornerDisplaySlots;
	private Text NewHighScore;
	private Animator MenuAnim;
	private GPG GPG;
	private bool Unlocked50;
	private bool Unlocked100;
	private bool UnlockedMaster;
	[HideInInspector]
	public bool HighScoreThisRound;

	public bool HasShownAd;

	// Use this for initialization
	void Start () {

		NewHighScore = GameObject.Find ("New High Score").GetComponent<Text> ();

		ActiveScheme = Schemes [PlayerPrefs.GetInt ("ActiveDotScheme", 4)];

		if (GameObject.FindGameObjectWithTag("AdMobManager") == null) {

			AdMobManager = Instantiate(AdMobManager) as GameObject;

		} else {

			AdMobManager = GameObject.FindGameObjectWithTag("AdMobManager");

		}

		try {
			GPG = GameObject.FindGameObjectWithTag("GPGManager").GetComponent<GPG>();
		} catch {
			Debug.LogWarning ("Couldn't find GPGS Manager, creating one for you");

			GPG = Instantiate (GPGManager).GetComponent<GPG>();
		}
		
		AudioListener.volume = PlayerPrefs.GetInt("Sound");

		if (PlayerPrefs.GetInt("Difficulty") == 0) {

			HighScore = PlayerPrefs.GetInt("HighScore.Easy");

			Diff = Difficulty.Easy;

		} else if (PlayerPrefs.GetInt("Difficulty") == 1) {

			HighScore = PlayerPrefs.GetInt("HighScore.Medium");

			Diff = Difficulty.Medium;

		} else {

			HighScore = PlayerPrefs.GetInt("HighScore.Hard");

			Diff = Difficulty.Hard;

		}

		try {

			GPG.SubmitScore(PlayerPrefs.GetInt("HighScore.Easy"), Difficulty.Easy);
			GPG.SubmitScore(PlayerPrefs.GetInt("HighScore.Medium"), Difficulty.Medium);
			GPG.SubmitScore(PlayerPrefs.GetInt("HighScore.Hard"), Difficulty.Hard);

		} catch {

		}

		MenuAnim = GameObject.Find("PauseMenu").GetComponent<Animator>();
		ScoreDisplaySlots = GameObject.Find("ScoreDisplay").GetComponentsInChildren<Text>();
		CornerDisplaySlots = GameObject.Find("CornerDisplay").GetComponentsInChildren<Text>();



		SetPause(false);

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape)){

			TogglePause();

		}

		if (Paused) {

			Time.timeScale = 0;

		} else {

			Time.timeScale = 1;

		}

		DisplayScore = Score.ToString().PadLeft(3, '0');

		Digits = DisplayScore.ToString().ToCharArray();

		if (Score >= HighScore) {

			HighScoreThisRound = true;

		}

		int i = 0;

		while (i < ScoreDisplaySlots.Length) {

			if (Score >= 0){

				ScoreDisplaySlots[i].text = Digits[i].ToString();
				CornerDisplaySlots[i].text = Digits[i].ToString();

				CornerDisplaySlots [i].color = Color.white;

			} else {

				CornerDisplaySlots [i].color = Color.red;

			}



			if (HighScoreThisRound) {

				ScoreDisplaySlots [i].GetComponent<AnimateScoreSlot> ().HighScore = true;
				CornerDisplaySlots [i].GetComponent<AnimateScoreSlot> ().HighScore = true;
				NewHighScore.color = Color.Lerp (NewHighScore.color, Color.white, Time.unscaledDeltaTime * 10);

			} else {

				ScoreDisplaySlots [i].GetComponent<AnimateScoreSlot> ().HighScore = false;
				CornerDisplaySlots [i].GetComponent<AnimateScoreSlot> ().HighScore = false;
				NewHighScore.color = Color.Lerp (NewHighScore.color, Color.clear, Time.unscaledDeltaTime * 10);

			}

			i ++;

		}

		//Manage Achievements

		if (GPG.SignedIn) { 

			if (Score >= 500 && !UnlockedMaster) {

				//Unlock Master Achievement

				if (Diff == Difficulty.Hard) {
					// unlock achievement
					GPG.UnlockAchievement(Achievements.Master);
				}

				UnlockedMaster = true;

			} else if (Score >= 100 && !Unlocked100) {

				//Unlock >100 Score Achievement
				
				if (Diff == Difficulty.Easy) {
					// unlock achievement
					GPG.UnlockAchievement(Achievements.Easy100);
				} else if (Diff == Difficulty.Medium) {
					// unlock achievement
					GPG.UnlockAchievement(Achievements.Med100);
				} else {
					// unlock achievement
					GPG.UnlockAchievement(Achievements.Hard100);
				}

				Unlocked100 = true;

			} else if (Score >= 50 && !Unlocked50) {

				//Unlock >50 Score Achievement

				if (Diff == Difficulty.Easy) {
					// unlock achievement
					GPG.UnlockAchievement(Achievements.Easy50);
				} else if (Diff == Difficulty.Medium) {
					// unlock achievement
					GPG.UnlockAchievement(Achievements.Med50);
				} else {
					// unlock achievement
					GPG.UnlockAchievement(Achievements.Hard50);
				}

				Unlocked50 = true;

			}

		}

		if (Score > HighScore) {
			HighScore = Score;
			i = 0;
			SaveHighScore(PlayerPrefs.GetInt("Difficulty"));
		} else {

		}

		if (!HasShownAd && PlayerPrefs.GetInt("Ads") == 1) {
			
			RequestGoogleAd ();

			HasShownAd = true;

		}
	
	}

	public void ResetScore () {

		PlayerPrefs.Save();

		Score = 0;

	}

	public void SaveHighScore (int Difficulty) {

		if (Difficulty == 0) {

			PlayerPrefs.SetInt("HighScore.Easy", HighScore);

		} else if (Difficulty == 1) {

			PlayerPrefs.SetInt("HighScore.Medium", HighScore);

		} else {

			PlayerPrefs.SetInt("HighScore.Hard", HighScore);

		}

		PlayerPrefs.Save();

	}

	public void TogglePause () {

		Paused = !Paused;

		MenuAnim.SetBool("Paused", Paused);

	}

	public void SetPause (bool SetTo) {

		Paused = SetTo;

		MenuAnim.SetBool("Paused", Paused);

	}

	public void RequestGoogleAd () {

		GPG.GetComponent<AdMobManager>().RequestBanner();

	}

	public void UploadScore (int Difficulty) {

		if (Difficulty == 0) {

			if (GPG.SignedIn) {
				GPG.SubmitScore(HighScore, global::Difficulty.Easy);
			}

		} else if (Difficulty == 1) {

			if (GPG.SignedIn) {
				GPG.SubmitScore(HighScore, global::Difficulty.Medium);
			}

		} else {

			if (GPG.SignedIn) {
				GPG.SubmitScore(HighScore, global::Difficulty.Hard);
			}

		}

	}

}
