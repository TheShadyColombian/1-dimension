﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class SceneLoader : MonoBehaviour {

	public bool SplashTransition;
	public float SplashDuration;

	private bool Loading;
	private bool Game;
	private int TargetDiff;
	private float SplashTimer;
	private Animator Fader;

	private bool HasSentAnalytics_StartedGame;
	private bool HasSentAnalytics_FirstTimePlayer;

	// Use this for initialization
	void Start () {

		Fader = GameObject.Find("UI.Fade").GetComponent<Animator>();

		if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Menu")) {

			//GetComponent<AdMobManager>().RequestInterstitial();

		}
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Loading == true) {

			Fader.SetBool("Fade", true);

			if (Fader.GetCurrentAnimatorStateInfo(0).IsName("Idle")) {

				if (Game) {

					PlayerPrefs.SetInt("Difficulty", TargetDiff);
					PlayerPrefs.Save();

					SceneManager.LoadScene("1D");

				} else {

					SceneManager.LoadScene("Menu");
				
				}

			}

		}

		if (SplashTransition) {

			SplashTimer += Time.deltaTime;

			if (SplashTimer >= SplashDuration) {

				Fader.SetBool("Fade", true);

				if (Fader.GetCurrentAnimatorStateInfo(0).IsName("Idle")) {

					SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

				}

			}

			if (!HasSentAnalytics_FirstTimePlayer && PlayerPrefs.GetInt("FirstTime") == 0) {

				Debug.Log (" [[[[[[[----->> Attempting to send Analytics to Unity servers");

				Analytics.CustomEvent("Game Started", new Dictionary<string, object> {

					{"First Time Player!", "...Welcome to 1D"}

				});

				Debug.Log("Sending Analytics to Unity Servers... (Sending First Time Player");

				PlayerPrefs.SetInt("FirstTime", 1);
				PlayerPrefs.Save();

				HasSentAnalytics_FirstTimePlayer = true;

				Debug.Log (" [[[[[[[----->> Successfully sent Analytics to Unity servers!");

			}

		}
	
	}

	public void LoadGame (int Difficulty) {

		TargetDiff = Difficulty;
		Loading = true;
		Game = true;

		Time.timeScale = 1;

	}

	public void MainMenu () {

		Loading = true;
		Game = false;

	}

}
