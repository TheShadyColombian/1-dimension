﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingManager : MonoBehaviour {

	public Transform FXPool;
	public Transform DotPool;

	public int FXInstances;
	public int DotInstances;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#if UNITY_EDITOR
    void OnGUI() {
		//Draw labels:
		GUI.Label (new Rect (2, 60, 100, 20), "Total Instances: ");
		GUI.Label (new Rect (2, 30, 100, 20), "Dot Instances: ");
		GUI.Label (new Rect (2, 10, 100, 20), "FX Instances: ");
		//Draw Values:
		GUI.Label (new Rect (100, 60, 100, 20), (FXInstances+DotInstances).ToString ());
		GUI.Label (new Rect (100, 30, 100, 20), DotInstances.ToString ());
		GUI.Label (new Rect (100, 10, 100, 20), FXInstances.ToString ());
		//Draw Active Values;
		GUI.Label (new Rect (120, 60, 100, 20), "(" + ((FXInstances + DotInstances) - (FXPool.childCount + DotPool.childCount)) + " active)");
		GUI.Label (new Rect (120, 30, 100, 20), "(" + (DotInstances - DotPool.childCount) + " active)");
		GUI.Label (new Rect (120, 10, 100, 20), "(" + (FXInstances - FXPool.childCount) + " active)");
	}
	#endif

	public void ArchiveObject (GameObject obj) {

		if (obj.GetComponent <DotManager>()) {
			obj.GetComponent<DotManager> ().Active = false;
			obj.transform.parent = DotPool;
			obj.transform.Translate (Vector3.left * 50);
			obj.GetComponent<DotManager> ().ResetDot ();
		} else {
			obj.transform.parent = FXPool;
		}

		obj.SetActive (false);
		
	}

	public GameObject ReInstance (GameObject obj) {

		if (obj.GetComponent <DotManager>()) {

			if (DotPool.childCount > 0) {

				Transform dot = DotPool.GetChild (0);
				dot.transform.position = Vector3.zero;
				dot.SetParent (null);
				dot.GetComponent<DotManager> ().Start ();
				dot.gameObject.SetActive (true);
				return dot.gameObject;

			} else

				DotInstances++;
			
		} else {

			if (FXPool.childCount > 0) {

				FXPool.GetChild (0).gameObject.SetActive (true);
				FXPool.GetChild (0).GetComponent<Animator> ().SetTrigger ("Reset");
				return FXPool.GetChild (0).gameObject;

			} else

				FXInstances++;

		}

		return Instantiate (obj);

	}

}
