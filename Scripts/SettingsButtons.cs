﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using UnityEngine.Purchasing;
using System;
using System.Collections;
using System.Collections.Generic;

public enum Toggles {

	sound, ads

}

public class SettingsButtons : MonoBehaviour {

	public GameObject AdMobManager;
	public Toggles ToggleType;
	public bool isToggle;
	public bool Value;
	public Image Img;
	public Sprite EnabledSprite;
	public Sprite DisabledSprite;

	private bool ButtonState;
	private bool CBSState;

	//Ads Stuff 

	public string zoneId;

	// Use this for initialization
	void Start () {

		if (isToggle) {

			FetchValue();

		}

		try{

			GameObject.Find("MainMenu").GetComponent<Animator>().SetBool("Visible", true);
			GameObject.Find("ExtraMenu").GetComponent<Animator>().SetBool("Visible", false);

		} catch {

		}

		if (PlayerPrefs.GetInt("HasSeenExtras", 0) == 0) {

			PlayerPrefs.SetInt("HasSeenExtras", 0);
			PlayerPrefs.Save();

		}

	}
	
	// Update is called once per frame
	void Update () {


	
	}

	public void ToggleValue () {

		Value = !Value;

		if (Value == true) {
			Img.sprite = EnabledSprite;
		} else {
			Img.sprite = DisabledSprite;
		}

	}

	public void FetchValue () {

		if (ToggleType == Toggles.ads) {

			Value = Convert.ToBoolean(PlayerPrefs.GetInt("Ads", 1));

			SetAdsSettings(false);

		} 

		if (ToggleType == Toggles.sound) {

			Value = Convert.ToBoolean(PlayerPrefs.GetInt("Sound", 1));

			SetSoundSettings();

		}

		if (Value == true) {
			Img.sprite = EnabledSprite;
		} else {
			Img.sprite = DisabledSprite;
		}

	}

	public void SetSoundSettings () {
		PlayerPrefs.SetInt("Sound", Convert.ToInt16(Value));
		PlayerPrefs.Save();
		AudioListener.volume = PlayerPrefs.GetInt("Sound");
	}

	public void SetAdsSettings (bool Express) {
		PlayerPrefs.SetInt("Ads", Convert.ToInt16(Value));
		PlayerPrefs.Save();

		/*

		if (AdMobManager == null) {

			if (GameObject.FindGameObjectWithTag("AdMobManager") == null) {

				AdMobManager = GameObject.Instantiate(AdMobManager) as GameObject;

			} else {

				AdMobManager = GameObject.FindGameObjectWithTag("AdMobManager");

			}

		}

		if (Value) {

			//AdMobManager.GetComponent<AdMobPlugin>().Show();

		} else {

			//AdMobManager.GetComponent<AdMobPlugin>().Hide();

		}

*/

		if (Express) {
			if (Value == true) {
				ExpressAppreciation();
				GameObject.Find("Thanks").GetComponent<Animator>().ResetTrigger("FastCancel");
				GameObject.Find("YouSuck").GetComponent<Animator>().SetTrigger("FastCancel");
			} else {
				ExpressRegret();
				GameObject.Find("YouSuck").GetComponent<Animator>().ResetTrigger("FastCancel");
				GameObject.Find("Thanks").GetComponent<Animator>().SetTrigger("FastCancel");
			}
		}



	}

	public void ShowSite () {
		Application.OpenURL("https://theshadycolombian.github.io");
	}

	public void SendEmail () {
		Application.OpenURL("mailto:juancallejasdesign@gmail.com");
	}

	public void Rate () {
		if (Application.platform == RuntimePlatform.Android){
			Application.OpenURL("market://details?id=com.JuanCallejas.OneDimension");
		}
		Application.OpenURL("https://play.google.com/store/apps/details?id=com.JuanCallejas.OneDimension");
		
		ExpressAppreciation();
	}

	public void Donate () {

		Analytics.CustomEvent("Donation", new Dictionary<string, object> {

			{"Attempted to donate money", null}

		});

		GetComponent<Purchaser>().BuyConsumable();

	}

	public void ToggleSubButtons (bool ToggledOff) {

		if (!ToggledOff) {

			GameObject[] OtherButtons = GameObject.FindGameObjectsWithTag("MenuButtons");
			int i = 0;

			while (i < OtherButtons.Length) {

				if (OtherButtons[i] != gameObject){

					OtherButtons[i].GetComponent<SettingsButtons>().ToggleSubButtons(true);

				}

				i++;

			}

			ButtonState = !ButtonState;

			try {

				GameObject.Find("SideButtonsAnchor." + name).GetComponent<Animator>().ResetTrigger("Show");
				GameObject.Find("SideButtonsAnchor." + name).GetComponent<Animator>().ResetTrigger("Hide");

				if (ButtonState == true) {
					GameObject.Find("SideButtonsAnchor." + name).GetComponent<Animator>().SetTrigger("Show");
				} else {
					GameObject.Find("SideButtonsAnchor." + name).GetComponent<Animator>().SetTrigger("Hide");
				}

				GameObject.Find("DifficultyIcon." + name).GetComponent<Animator>().SetBool("Mode", ButtonState);
				GameObject.Find("DifficultyLabel." + name).GetComponent<Animator>().SetBool("Mode", ButtonState);


			} catch {

				Debug.LogWarning("Current Object does not have sub buttons of it's own");

			}

		} else {

			ButtonState = false;

			try {
				GameObject.Find("SideButtonsAnchor." + name).GetComponent<Animator>().SetTrigger("Hide");
				GameObject.Find("DifficultyIcon." + name).GetComponent<Animator>().SetBool("Mode", false);
				GameObject.Find("DifficultyLabel." + name).GetComponent<Animator>().SetBool("Mode", false);
			} catch {
				Debug.LogWarning("Attempting to hide sub buttons failed. Are you sure the object \"" + name + "\" has a set of sub buttons?");
			}

		}

	}

	public void ShowAchievements () {

		GameObject.FindGameObjectWithTag("GPGManager").GetComponent<GPG>().ShowAchievements();

	}

	public void ShowLeaderboards () {

		GameObject.FindGameObjectWithTag("GPGManager").GetComponent<GPG>().ShowLeaderBoard(0);

	}

	public void ShowLeaderboard (int Leaderboard) {

		GameObject.FindGameObjectWithTag("GPGManager").GetComponent<GPG>().ShowLeaderBoard(Leaderboard);

	}

	public void ToggleExtraMenu () {
		GameObject.Find("MainMenu").GetComponent<Animator>().SetBool("Visible", !Value);
		GameObject.Find("ExtraMenu").GetComponent<Animator>().SetBool("Visible", Value);
		PlayerPrefs.SetInt("HasSeenExtras", 1);
	}

	public void ShowAd () {

		FindObjectOfType<AdMobManager> ().RequestInterstitial ();

	}

	public void Wipe_Initialize () {

		GameObject.Find("Erase.Confirmation").GetComponent<Animator>().SetTrigger("Open");

	}

	public void Wipe_Confirm () {
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();

		GameObject.Find("MainMenu").GetComponent<Animator>().SetBool("Visible", true);
		GameObject.Find("ExtraMenu").GetComponent<Animator>().SetBool("Visible", false);

		GameObject.Find("Erase.Confirmation").GetComponent<Animator>().SetTrigger("Close");
	}

	public void Wipe_Cancel () {

		GameObject.Find("Erase.Confirmation").GetComponent<Animator>().SetTrigger("Close");

	}

	public void ColourBlind_ShowPanel () {

		GameObject.Find ("ColourBlind.Panel").GetComponent<Animator> ().SetTrigger ("Open");

	}

	public void ColourBlind_HidePanel () {

		GameObject.Find ("ColourBlind.Panel").GetComponent<Animator> ().SetTrigger ("Close");
		Debug.Log ("Saving player prefs");
		PlayerPrefs.Save ();

	}

	public void ExpressLOADING () {
		GameObject.Find("Loading").GetComponent<Animator>().SetTrigger("Show");
	}
	public void ExpressAppreciation () {
		GameObject.Find("Thanks").GetComponent<Animator>().SetTrigger("Show");
	}
	public void ExpressRegret () {
		GameObject.Find("YouSuck").GetComponent<Animator>().SetTrigger("Show");
	}
	public void ExpressERROR () {
		GameObject.Find("Loading").GetComponent<Animator>().SetTrigger("FastCancel");
		GameObject.Find("Error").GetComponent<Animator>().SetTrigger("Show");
	}

}
