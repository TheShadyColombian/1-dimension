﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class DotScheme : ScriptableObject {

	public bool UseGlyphs;
	public Color [] Colours;
	public Sprite [] Glyphs;
	public Sprite UniversalGlyph;
	public int ID;

	public DotScheme () {

		Colours = new Color [6];
		for (int i = 0; i < Colours.Length; i++) {
			Colours [i] = Color.white;
		}

	}

}