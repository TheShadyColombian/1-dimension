﻿using UnityEngine;
using System.Collections;

public class TimsGame : MonoBehaviour {

	/*
     * steps:
     *  tell script where rigidbody is
     *  add force rb.addforce
     *  bind keys
     *  idk
     *  vector 2
     *  new vector2(x,y)
     *  vector.left * speed (auto does things)
     */

	public int speed = 5;
	Rigidbody2D rb;


	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update () {

		//GetKeyDown only returns true on the exact frame you press the key. OnKey runs EVERY frame the key is pressed
		//Also, Where's the method i showed you? This works, and you can keep it if you want, but the other way is much nicer

		if (Input.GetKeyDown(KeyCode.W))
		{
			rb.AddForce(Vector2.up * speed);
		}
		if (Input.GetKeyDown(KeyCode.S))
		{
			rb.AddForce(Vector2.down * speed);
		}
		if (Input.GetKeyDown(KeyCode.A))
		{
			rb.AddForce(Vector2.left * speed);
		}
		if (Input.GetKeyDown(KeyCode.D))
		{
			rb.AddForce(Vector2.right * speed);
		}
	}
}