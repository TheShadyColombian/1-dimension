﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class ImageSetColorFromParent : MonoBehaviour {

	Image Img;
	Text Txt;
	Image ParImg;

	// Use this for initialization
	void Start () {

		if (GetComponent<Image>() != null){

			Img = GetComponent<Image>();

		}

		if (GetComponent<Text>() != null){

			Txt = GetComponent<Text>();

		}

		ParImg = transform.parent.GetComponent<Image>();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Img != null){
			Img.color = ParImg.color;
		}

		if (Txt != null){
			Txt.color = new Color(ParImg.color.r , ParImg.color.g , ParImg.color.b , ParImg.color.a);
		}
	
	}
}
