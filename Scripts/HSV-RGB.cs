﻿using UnityEngine;
using System.Collections;

namespace ColorConvert {
		
	public sealed class HSV_RGB {
		

		/// <summary>
		/// Clamp a value to 0-255
		/// </summary>
		int Clamp(int i)
		{
			if (i < 0) return 0;
			if (i > 255) return 255;
			return i;
		}


	}

}