﻿using UnityEngine;
using System.Collections;

public enum ActiveMode {

	ifHasntWatchedAd,ifHasntDonated,ifHasntSeenExtras

}

public class PulseManager : MonoBehaviour {

	public ActiveMode Mode;

	private Animator Anim;

	// Use this for initialization
	void Start () {

		Anim = GetComponent<Animator>();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (PlayerPrefs.GetInt("HasWatchedAd") == 0 && Mode == ActiveMode.ifHasntWatchedAd) {

			Anim.SetBool("Pulse", true);

		} else if (PlayerPrefs.GetInt("HasWatchedAd") == 1 && Mode == ActiveMode.ifHasntWatchedAd) {

			Anim.SetBool("Puslse", false);

		}

		if (PlayerPrefs.GetInt("HasDonated") == 0 && Mode == ActiveMode.ifHasntDonated) {

			Anim.SetBool("Pulse", true);

		} else if (PlayerPrefs.GetInt("HasDonated") == 1 && Mode == ActiveMode.ifHasntDonated) {

			Anim.SetBool("Puslse", false);

		}

		if (PlayerPrefs.GetInt("HasSeenExtras") == 0 && Mode == ActiveMode.ifHasntSeenExtras) {

			Anim.SetBool("Pulse", true);

		} else if (PlayerPrefs.GetInt("HasSeenExtras") == 1 && Mode == ActiveMode.ifHasntSeenExtras) {

			Anim.SetBool("Pulse", false);

		}
	
	}

}
