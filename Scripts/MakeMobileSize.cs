﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeMobileSize : MonoBehaviour {

	public bool CorrectionalOnly = true;
	public Vector2 AspectRatio = new Vector2 (9, 16);
	[SerializeField]
	private Vector2 newRes;

	// Use this for initialization
	void Start () {

		if (CorrectionalOnly && Screen.height > Screen.width) return;

		Vector2 NewRes = new Vector2 (0, Screen.height);
		float width = Screen.height * (AspectRatio.x / AspectRatio.y);
		NewRes.x = width;

		newRes = NewRes;

		Screen.SetResolution ((int)NewRes.x, (int)NewRes.y, false, 60);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

}
