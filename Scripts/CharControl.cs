﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum Key {

	A,S,D,F,G

}

public class CharControl : MonoBehaviour {

	[SerializeField] private Key ActiveKey;
	public GameObject Dot;
	public GameObject FX;
	public bool SimulateMobile;
	public bool Dead;
	public int ID;
	public int NextID;
	[SerializeField] private Color[] ColorRange;
	public Color ActiveColor;
	public string[] Tips;
	public string[] ExtraTips;

	public AudioClip Begin;
	public AudioClip End;
	public AudioClip Boop;
	public AudioSource AS;

	private Creator MainCreator;
	private Animator HelpAnim;
	private Animator HelpFooter;
	private Manager Manager;
	private PoolingManager Pool;
	private GameObject OldDotInst;
	private GameObject DotInst;
	private GameObject FXInst;
	private GameObject Decor;
	private GameObject InputPanel;
	private Image Img;
	public bool Mobile;
	private bool MobileInputDown;
	private bool Creating;
	private bool isFist;
	private bool PlayedDiedFX = true;
	private int LoserCount;
	private float TimeDead;

	// Use this for initialization
	void Start () {

		Pool = FindObjectOfType<PoolingManager> ();

		Manager = GameObject.Find("Manager").GetComponent<Manager>();
		MainCreator = GameObject.FindGameObjectWithTag("Creator").GetComponent<Creator>();
		Img = GetComponent<Image>();
		Decor = transform.GetChild(0).gameObject;
		ID = 1;
		HelpFooter = GameObject.Find("HelpFooter").GetComponent<Animator>();

		InputPanel = GameObject.FindGameObjectWithTag("InputPanel");

		for (int i = 0; i < InputPanel.transform.childCount; i++)

			if (!Manager.ActiveScheme.UseGlyphs)

				InputPanel.transform.GetChild (i).GetChild (0).GetChild (1).gameObject.SetActive (false);

			else

				InputPanel.transform.GetChild (i).GetChild (0).GetChild (0).gameObject.SetActive (false);
		



		HelpAnim = InputPanel.GetComponent<Animator> ();

		if (GameObject.Find("Creator").GetComponent<Creator>().Diff == Difficulty.Easy) {

			InputPanel.transform.GetChild (InputPanel.transform.childCount - 1).gameObject.SetActive (false);
			InputPanel.transform.GetChild (InputPanel.transform.childCount - 2).gameObject.SetActive (false);

		} else if (GameObject.Find ("Creator").GetComponent<Creator> ().Diff == Difficulty.Medium) {

			InputPanel.transform.GetChild (InputPanel.transform.childCount - 1).gameObject.SetActive (false);

		}

		for (int i = 0; i < MainCreator.ActiveColors.Length; i++) {

			InputPanel.transform.GetChild (i).GetComponent<Image> ().color = MainCreator.ActiveColors [i];

		}

		AS = GetComponent<AudioSource>();

		if (Application.isMobilePlatform || SimulateMobile) {

			ShowHelp(true);

			Mobile = true;

		}

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.anyKeyDown && !(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.G) || Input.GetButtonDown("Fire1"))) {

			ShowHelp(false);

		}

		//If the Color Range contains less than two colors, try to fetch it again from the creator

		if (ColorRange.Length < 2){

			ColorRange = MainCreator.ActiveColors;

		}

		if (!Manager.Paused) {

			if (!Dead) {

				TimeDead = 0;

				//Store the previous ID for claping

				int OldID = ID;

				//Assing the current ID according to the button last pressed from A to G, or use the custom enum if it is mobile

				if (Mobile) {

					ID = (int)ActiveKey + 1;

					if (MobileInputDown == true){

						Creating = true;

						MobileInputDown = false;

						Debug.Log ("Pressed Mobile Key");

					}

				} else {

					if (Input.GetKeyDown(KeyCode.A)) {
						ID = 1;
						Creating = true;
					} else if (Input.GetKeyDown(KeyCode.S)) {
						ID = 2;
						Creating = true;
					} else if (Input.GetKeyDown(KeyCode.D)) {
						ID = 3;
						Creating = true;
					} else if (Input.GetKeyDown(KeyCode.F)) {
						ID = 4;
						Creating = true;
					} else if (Input.GetKeyDown(KeyCode.G)) {
						ID = 5;
						Creating = true;
					}

				}

				//Clamp the ID if it is not included in the current difficulty

				if (ID > ColorRange.Length) {

					ID = OldID;

				}

				//Make the currently active color the color in ColorRange with the selected ID, and set the player's color to the currently selected color

				ActiveColor = ColorRange[ID - 1];

				Img.color = ActiveColor;

				//Create the prefab

				if (Creating) {

					Create (ID - 1);

					Creating = false;

					NextID += 1;

				}

				if (DotInst == null) {

					isFist = true;

				}

			} else {

				//If the player pushes a button, restart the game

				if (Mobile) {

					if (ActiveKey == Key.A) {
						ID = 1;
					} else if (ActiveKey == Key.S) {
						ID = 2;
					} else if (ActiveKey == Key.D) {
						ID = 3;
					} else if (ActiveKey == Key.F) {
						ID = 4;
					} else if (ActiveKey == Key.G) {
						ID = 5;
					}

					if (MobileInputDown == true){

						Creating = true;

						MobileInputDown = false;

					}

				} else {

					if (Input.GetKeyDown(KeyCode.A)) {
						ID = 1;
						Creating = true;
					} else if (Input.GetKeyDown(KeyCode.S)) {
						ID = 2;
						Creating = true;
					} else if (Input.GetKeyDown(KeyCode.D)) {
						ID = 3;
						Creating = true;
					} else if (Input.GetKeyDown(KeyCode.F)) {
						ID = 4;
						Creating = true;
					} else if (Input.GetKeyDown(KeyCode.G)) {
						ID = 5;
						Creating = true;
					}

				}



				//Make a delay for restarting the game

				TimeDead += Time.deltaTime;

				//If the timer is up, and the player attempts to create a dot, start the game

				if (Creating && TimeDead >= 1){

					Revive();

					Creating = false;

					Manager.ResetScore();

				}

				Creating = false;

			}

		}
	
	}

	void Create (int Hue) {

		if (!isFist) {

			OldDotInst = DotInst;

		}

		DotInst = 
			//Instantiate (Dot); 
			Pool.ReInstance (Dot);

		DotInst.transform.SetParent(transform.parent.parent);

		DotInst.transform.position = transform.position;

		DotInst.GetComponent<SpriteRenderer> ().color = ColorRange [Hue];
		DotInst.GetComponent<DotManager> ().ToneID = Hue;

		if (Manager.ActiveScheme.UseGlyphs) {
			DotInst.transform.GetChild (0).GetComponent<SpriteRenderer> ().sprite = Manager.ActiveScheme.Glyphs [ID - 1];
		}
		if (Manager.ActiveScheme.UniversalGlyph != null) {
			DotInst.transform.GetChild (1).GetComponent<SpriteRenderer> ().sprite = Manager.ActiveScheme.UniversalGlyph;
		}

		DotInst.GetComponent<DotManager>().Active = true;

		DotInst.GetComponent<DotManager>().ID = ID;

		//DotInst.GetComponent<RectTransform>().localScale = Vector3.one;

		if (!isFist) {

			DotInst.GetComponent<DotManager>().NextDot = OldDotInst;

		}

		DotInst.GetComponent<DotManager>().Bullet = true;

		FXInst = Pool.ReInstance (FX);

		FXInst.transform.SetParent(transform.parent.parent);

		FXInst.transform.position = transform.position;

		FXInst.name = "CharFX";

		FXInst.GetComponentInChildren<SpriteRenderer> ().color = ColorRange [Hue];

		//FXInst.GetComponent<RectTransform>().localScale = Vector3.one;

		isFist = false;

		//AudioSource.PlayClipAtPoint(Boop, Vector2.zero);

		AS.clip = Boop;
		AS.Play();
		float p = ID;
		AS.pitch = p/10 + 0.9f;

	}

	public void Die () {

		Manager.UploadScore(PlayerPrefs.GetInt("Difficulty"));

		Decor.GetComponent<Image>().color = Color.red;
		Img.color = Color.white;

		GameObject.Find ("StartPanel").GetComponent<Animator> ().SetBool ("Show", true);

		Dead = true;
		MainCreator.CharIsDead = true;

		if (!PlayedDiedFX) {

			AudioSource.PlayClipAtPoint(End, Vector2.zero);
			AS.clip = End;
			AS.pitch = 1;
			AS.Play();

			PlayedDiedFX = true;

			//Other non-sound-related stuff

			if (Manager.Score <= 3){

				LoserCount ++;

				if (LoserCount >= 3) {

					HelpFooter.SetTrigger("Extra");
					if (Random.Range(0, 3) == 0){
						HelpFooter.gameObject.GetComponentInChildren<Text>().text = Tips[Random.Range(0, Tips.Length)];
					} else {
						HelpFooter.gameObject.GetComponentInChildren<Text>().text = ExtraTips[Random.Range(0, ExtraTips.Length)];
					}

				} else if (LoserCount >= 1) {
					
					HelpFooter.SetTrigger("Show");
					HelpFooter.gameObject.GetComponentInChildren<Text>().text = Tips[Random.Range(0, Tips.Length)];

				}

			} else {

				LoserCount = 0;

			}

		}

		Manager.HasShownAd = false;

	}

	public void Revive () {

		Decor.GetComponent<Image>().color = Color.black;
		Img.color = ActiveColor;

		Dead = false;

		GameObject.Find ("StartPanel").GetComponent<Animator> ().SetBool ("Show", false);

		MainCreator.CharIsDead = false;
		Manager.HighScoreThisRound = false;
		Manager.ResetScore ();

		if (PlayedDiedFX) {

			AudioSource.PlayClipAtPoint(Begin, Vector2.zero);

			AS.clip = Begin;
			AS.pitch = 1;
			AS.Play();

			PlayedDiedFX = false;

		}

		FXInst = Pool.ReInstance (FX);

		FXInst.transform.SetParent(transform.parent);

		FXInst.transform.position = transform.position;

		FXInst.name = "CharFX";

		FXInst.GetComponentInChildren<SpriteRenderer>().color = Color.white;

		//FXInst.GetComponent<RectTransform>().localScale = Vector3.one;

	}

	public void ShowHelp (bool Permanent) {

		if (Permanent) {

			HelpAnim.SetBool ("Locked", true);

		} else {

			if (HelpAnim.GetCurrentAnimatorStateInfo (0).IsName ("InputPanel.HelpFade")) {

				HelpAnim.SetTrigger ("Appear");

			}

		}


	}

	public void UseButton (int UseKey) {

		UseKey = Mathf.Clamp(UseKey, 0, ColorRange.Length);

		Debug.Log("Changing Key to " + UseKey);

		if (UseKey >= 1) {

			ActiveKey = (Key) (UseKey - 1);

		}

		MobileInputDown = true;

	}

}
