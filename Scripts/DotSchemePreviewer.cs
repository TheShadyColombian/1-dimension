﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class DotSchemePreviewer : MonoBehaviour {

	public DotScheme SchemeToPreview;
	public bool ShowGlyphs;

	private Image [] Images;
	private Image [] Glyphs;

	// Use this for initialization
	void Start () {

		Images = new Image [transform.childCount];
		Glyphs = new Image [transform.childCount];

		for (int i = 0; i < transform.childCount; i ++) {
			Images [i] = transform.GetChild (i).GetComponent<Image> ();
			Glyphs [i] = Images[i].transform.GetChild (0).GetChild (0).GetComponent<Image> ();
		}
		
	}
	
	// Update is called once per frame
	void Update () {

		ShowGlyphs = SchemeToPreview.UseGlyphs;

		if (Application.isPlaying) {

			for (int i = 0; i < Images.Length; i++) {

				Images [i].color = Color.Lerp (Images [i].color, SchemeToPreview.Colours [i], Time.unscaledDeltaTime * 5);

				if (ShowGlyphs) 
					Glyphs[i].color = Color.Lerp (Glyphs [i].color, Color.white, Time.unscaledDeltaTime * 5);
				else 
					Glyphs[i].color = Color.Lerp (Glyphs [i].color, Color.clear, Time.unscaledDeltaTime * 5);

			}

		} else {

			for (int i = 0; i < Images.Length; i++) {

				Images [i].color = SchemeToPreview.Colours [i];

			}
		
		}
		
	}

	public void SetScheme (DotScheme newScheme) {

		SchemeToPreview = newScheme;

		PlayerPrefs.SetInt ("ActiveDotScheme", newScheme.ID);
		Debug.LogWarning ("Setting active dot scheme to scheme " + newScheme + " with ID #" + newScheme.ID);

	}

}
