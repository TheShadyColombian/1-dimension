﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Purchasing;
using UnityEngine.SocialPlatforms;
using System.Collections;
using System;
#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

public enum Achievements {

	Easy50,Easy100,Med50,Med100,Hard50,Hard100,Master

}

public class GPG : MonoBehaviour {
	
	public bool SignedIn;

	private string Ach_Easy_50 = "CgkIiIeYtM0eEAIQAw";
	private string Ach_Easy_100 = "CgkIiIeYtM0eEAIQBA";
	private string Ach_Medium_50 = "CgkIiIeYtM0eEAIQBQ";
	private string Ach_Medium_100 = "CgkIiIeYtM0eEAIQBg";
	private string Ach_Hard_50 = "CgkIiIeYtM0eEAIQBw";
	private string Ach_Hard_100 = "CgkIiIeYtM0eEAIQCA";
	private string Ach_Master = "CgkIiIeYtM0eEAIQCQ";

	private string[] LeaderboardIDs = new string[3];

	private bool HasUploaded;
	private bool HasToldToUpload;

	// Use this for initialization
	void Start () {

		#if UNITY_ANDROID

		LeaderboardIDs[0] = "CgkIiIeYtM0eEAIQCg";
		LeaderboardIDs[1] = "CgkIiIeYtM0eEAIQCw";
		LeaderboardIDs[2] = "CgkIiIeYtM0eEAIQDA";
		PlayGamesPlatform.Activate();
		DontDestroyOnLoad(gameObject);

		//Initialize Google Play Games Services  

		Authenticate();
		#endif
	}
	
	// Update is called once per frame
	void Update () {

		#if UNITY_ANDROID
		if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Menu") && !HasToldToUpload){ 
			HasUploaded = false;
			HasToldToUpload = true;
		}

		if (!HasUploaded && SignedIn) {

			//Submit offline Achievements

			if (PlayerPrefs.GetInt("HighScore.Easy") >= 100) {
				UnlockAchievement(Achievements.Easy100);
			}
			if (PlayerPrefs.GetInt("HighScore.Easy") >= 50) {
				UnlockAchievement(Achievements.Easy50);
			}



			if (PlayerPrefs.GetInt("HighScore.Medium") >= 100) {
				UnlockAchievement(Achievements.Med100);
			} 
			if (PlayerPrefs.GetInt("HighScore.Medium") >= 50) {
				UnlockAchievement(Achievements.Med50);
			}



			if (PlayerPrefs.GetInt("HighScore.Hard") >= 500) {
				UnlockAchievement(Achievements.Master);
			} 
			if (PlayerPrefs.GetInt("HighScore.Hard") >= 100) {
				UnlockAchievement(Achievements.Hard100);
			} 
			if (PlayerPrefs.GetInt("HighScore.Hard") >= 50) {
				UnlockAchievement(Achievements.Hard50);
			}

			//Save offline highscores

			SubmitScore(PlayerPrefs.GetInt("HighScore.Easy", 0), Difficulty.Easy);
			SubmitScore(PlayerPrefs.GetInt("HighScore.Medium", 0), Difficulty.Medium);
			SubmitScore(PlayerPrefs.GetInt("HighScore.Hard", 0), Difficulty.Hard);

			HasUploaded = true;

		}

		#endif
	
	}

	public void Authenticate () {

		#if UNITY_ANDROID

		if (!PlayGamesPlatform.Instance.IsAuthenticated ()){

			// authenticate user:
			Social.localUser.Authenticate ((bool success) => {

				Debug.Log ("Success! You are now logged in");

				SignedIn = success;

			});
			
		}

		#endif

	}

	public void SubmitScore (int Score, Difficulty Diff)  {

		#if UNITY_ANDROID

		if (!SignedIn) {

			Authenticate();

			Debug.Log ("User is not signed in, and so scores will not be uploaded");

		} else {

			if (PlayGamesPlatform.Instance.IsAuthenticated ()) {

				if (Diff == Difficulty.Easy) {
					Social.ReportScore (Score, LeaderboardIDs [0], (bool success) => {
						// handle success or failure
					});
				} else if (Diff == Difficulty.Medium) {
					Social.ReportScore (Score, LeaderboardIDs [1], (bool success) => {
						// handle success or failure
					});
				} else if (Diff == Difficulty.Hard) {
					Social.ReportScore (Score, LeaderboardIDs [2], (bool success) => {
						// handle success or failure
					});
				}

			} else {
				
				Social.localUser.Authenticate ((bool authSuccess) => {

					if (Diff == Difficulty.Easy) {
						Social.ReportScore (Score, LeaderboardIDs [0], (bool success) => {
							// handle success or failure
						});
					} else if (Diff == Difficulty.Medium) {
						Social.ReportScore (Score, LeaderboardIDs [1], (bool success) => {
							// handle success or failure
						});
					} else if (Diff == Difficulty.Hard) {
						Social.ReportScore (Score, LeaderboardIDs [2], (bool success) => {
							// handle success or failure
						});
					}

				});

			}

		}

		#endif

		Debug.Log("Uploading Scores");

	}

	public void UnlockAchievement (Achievements Ach) {

		if (SignedIn){

			//Submit the high score to GPG

			Social.ReportProgress(AchToString(Ach), 100.0f, (bool success) => {
				if (success) {
					Debug.Log("Achievement Unlocked Succesfully");
				}
			});

		}

		Debug.Log("Unlocking Achievement " + Ach);

	}

	public void ShowAchievements () {

		if (!SignedIn) Authenticate ();

		if (SignedIn) {

			Social.ShowAchievementsUI ();

		}

	}

	public void ShowLeaderBoard (int LB) {

		#if UNITY_ANDROID

		if (!SignedIn) Authenticate();

		if (SignedIn) {

			if (LB == 0) {

				Social.ShowLeaderboardUI ();

			} else if (LB == 1) {

				PlayGamesPlatform.Instance.ShowLeaderboardUI ("CgkIiIeYtM0eEAIQCg");

			} else if (LB == 2) {

				PlayGamesPlatform.Instance.ShowLeaderboardUI ("CgkIiIeYtM0eEAIQCw");

			} else if (LB == 3) {

				PlayGamesPlatform.Instance.ShowLeaderboardUI ("CgkIiIeYtM0eEAIQDA");

			}

			Debug.Log ("Displaying Leaderboard");

		}

		#endif

	}

	public string AchToString (Achievements Ach) {

		if (Ach == Achievements.Easy50) {
			return Ach_Easy_50;

		} else if (Ach == Achievements.Easy100) {
			return Ach_Easy_100;

		
		} else if (Ach == Achievements.Med50) {
			return Ach_Medium_50;

		} else if (Ach == Achievements.Med100) {
			return Ach_Medium_100;


		} else if (Ach == Achievements.Hard50) {
			return Ach_Hard_50;

		} else if (Ach == Achievements.Hard100) {
			return Ach_Hard_100;

		
		} else if (Ach == Achievements.Master) {
			return Ach_Master;

		
		} else {
			return null;

		}

	}

}
