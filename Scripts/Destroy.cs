﻿using UnityEngine;
using System.Collections;

public enum Mode {

	TimeDelay,AnimationEnd

}

public class Destroy : MonoBehaviour {

	public Mode M;
	public float Delay;
	public bool UsePooling;

	private float DelayTimer;
	private Animator Anim;
	private PoolingManager Pool;

	// Use this for initialization
	void Start () {

		Pool = FindObjectOfType<PoolingManager> ();

		Anim = GetComponent<Animator>();
	
	}

	// Update is called once per frame
	void Update () {

		if (M == Mode.TimeDelay) {

			DelayTimer += Time.deltaTime;

			if (DelayTimer >= Delay)

				destroy ();

		}

		if (M == Mode.AnimationEnd) {

			if (Anim.GetCurrentAnimatorStateInfo (0).IsName ("Destroy"))

				destroy ();

		}
	
	}

	private void destroy () {

		if (UsePooling)
			Pool.ArchiveObject (gameObject);
		else
			Destroy (gameObject);

	}

}
