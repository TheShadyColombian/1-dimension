﻿using UnityEngine;
using UnityEngine.UI;
using ColorConvert;
using System.Collections;

public enum Difficulty {

	//Three levels of difficulties for the game

	Easy,Medium,Hard

}

public class Creator : MonoBehaviour {

	[Range (0, 3)] public int CustomDiff;
	public Difficulty Diff;
	public GameObject Dot;
	public GameObject FX;
	public float Spacing;
	public int ID;
	public int NextID;
	public Color[] Colors;
	public Color ActiveColor;
	public Color[] ActiveColors;
	public bool CharIsDead;

	public AudioClip Boop;

	private GameObject OldDotInst;
	private GameObject DotInst;
	private GameObject FXInst;
	private float SpacingTimer;
	private float OriginalSpacing;
	private float DiffVal;
	private int CurrentColorID;
	private Image Img;
	private Manager MainManager;
	private PoolingManager Pool;

	// Use this for initialization
	void Start () {

		Pool = FindObjectOfType<PoolingManager> ();

		MainManager = FindObjectOfType<Manager> ();

		Colors = MainManager.ActiveScheme.Colours;

		Diff = IntToDifficulty (PlayerPrefs.GetInt("Difficulty"));

		DiffVal = DifficultyToInt(Diff);

		Spacing = Spacing - (DiffVal/8);
		
		Img = GetComponent<Image>();

		if (Diff == Difficulty.Easy) {
			ActiveColors = new Color [3];
		} else if (Diff == Difficulty.Medium) {
			ActiveColors = new Color [4];
		} else if (Diff == Difficulty.Hard) {
			ActiveColors = new Color [5];
		}

		for (int i = 0; i < ActiveColors.Length; i++) ActiveColors [i] = Colors [i];

		ActiveColor = ActiveColors[Random.Range(0, ActiveColors.Length)];

		Img.color = ActiveColor;

		OriginalSpacing = Spacing;

		if (PlayerPrefs.GetInt("Ads") == 1) {
			
			GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -250);

			GameObject.Find("PauseButton").GetComponent<RectTransform>().anchoredPosition = new Vector2(-50, -150);

			GameObject.Find("CornerContainer").GetComponent<RectTransform>().anchoredPosition = new Vector2(50, -150);

		}
	
	}
	
	// Update is called once per frame
	void Update () {

		if (!MainManager.Paused) {

			if (!CharIsDead) {

				Spacing = Mathf.Clamp (Spacing/1.0001f, 0.2f, Mathf.Infinity);

				SpacingTimer += Time.deltaTime;

				if (SpacingTimer >= Spacing) {

					int Rand = Random.Range(0, 5);

					if (Rand % 2 == 0) {

						Create (CurrentColorID);

						transform.SetSiblingIndex(transform.parent.childCount);

					}

					CurrentColorID = Random.Range(0, ActiveColors.Length);

					ActiveColor = ActiveColors[CurrentColorID];

					Img.color = ActiveColor;

					SpacingTimer = 0;

				}

			} else {

				Spacing = OriginalSpacing;

			}

		}
	
	}

	void Create (int HueID) {

		OldDotInst = DotInst;

		DotInst = /*Instantiate (Dot);*/ Pool.ReInstance (Dot);

		DotInst.transform.SetParent(transform.parent.parent);

		DotInst.transform.position = transform.position;

		DotInst.GetComponent<SpriteRenderer>().color = ActiveColors[HueID];
		DotInst.GetComponent<DotManager> ().ToneID = HueID;

		if (MainManager.ActiveScheme.UseGlyphs) {
			DotInst.transform.GetChild (0).GetComponent<SpriteRenderer> ().sprite = MainManager.ActiveScheme.Glyphs [CurrentColorID];
		}
		if (MainManager.ActiveScheme.UniversalGlyph != null) {
			DotInst.transform.GetChild (1).GetComponent<SpriteRenderer> ().sprite = MainManager.ActiveScheme.UniversalGlyph;
		}

		DotInst.GetComponent<DotManager>().Active = true;
		DotInst.GetComponent<DotManager>().ID = HueID; 

		//DotInst.GetComponent<Transform>().localScale = Vector3.one;

		DotInst.layer = 9;

		if (OldDotInst != null) {
			
			OldDotInst.GetComponent<DotManager>().NextDot = DotInst;

		}

		FXInst = Pool.ReInstance (FX);

		FXInst.transform.SetParent(transform.parent.parent);

		FXInst.transform.position = transform.position;

		FXInst.GetComponentInChildren<SpriteRenderer>().color = ActiveColors[HueID];

		//FXInst.GetComponent<Transform>().localScale = Vector3.one;

		//AudioSource.PlayClipAtPoint(Boop, Vector2.zero);

		GetComponent<AudioSource>().clip = Boop;
		float p = CurrentColorID;
		GetComponent<AudioSource>().pitch = p/10 + 0.9f;
		GetComponent<AudioSource>().Play();

	}

	public int DifficultyToInt (Difficulty Diff) {

		if (Diff == Difficulty.Easy) {

			return 0;

		} else if (Diff == Difficulty.Medium) {

			return 1;

		} else {

			return 2;

		}

	}

	public Difficulty IntToDifficulty (int Diff) {

		if (Diff == 0) {

			return Difficulty.Easy;

		} else if (Diff == 1) {

			return Difficulty.Medium;

		} else {

			return Difficulty.Hard;
			
		}

	}

}