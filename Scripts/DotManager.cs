﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DotManager : MonoBehaviour {

	public LayerMask EnemyLayer;
	public GameObject ShatterFX;
	public int ID;
	public int ToneID;
	public float Speed;
	public float AccentAcceleration;
	public bool Active;
	public bool Bullet;
	public bool DebugLogs;
	public Transform Player;
	public Transform Creator;
	public GameObject NextDot;
	public GameObject CorrespondingDot;

	public AudioClip Boom;
	public AudioClip Bom;

	private static PoolingManager Pool;
	private static Manager Manager;
	private bool isFromPool;
	private float MinYPos;
	private float Accent;
	private float ModSpeed;
	private float ModAccent;
	private RaycastHit2D RCData;

	// Use this for initialization
	public void Start () {

		if (!Pool)
			Pool = FindObjectOfType<PoolingManager> ();
		
		if (!Manager)
			Manager = GameObject.Find("Manager").GetComponent<Manager>();

		if (transform.root.GetComponentInChildren<RectTransform> ())
			MinYPos = transform.root.position.y - (transform.root.GetComponentInChildren<RectTransform> ().sizeDelta.y / 2);
	
		Player = GameObject.FindGameObjectWithTag("Player").transform;
		Creator = GameObject.FindGameObjectWithTag("Creator").transform;

	}
	
	// Update is called once per frame
	void Update () {

		if (Active) {

			Accent += Time.deltaTime*AccentAcceleration;

			ModSpeed = Speed;
			ModAccent = Accent;
			//Debug.Log(ID);

			if (Bullet) {

				transform.position = new Vector2(transform.position.x, transform.position.y + ((ModSpeed*5) * Time.deltaTime));

				if (transform.position.y < MinYPos || transform.position.y > Creator.position.y) {

					if (DebugLogs) {

						if (transform.position.y < MinYPos)
							Debug.LogWarning ("Dot " + this.GetInstanceID () + " is below lower bound. Destroying...", gameObject);
						if (transform.position.y > Creator.position.y)
							Debug.LogWarning ("Dot " + this.GetInstanceID () + " is above upper bound. Destroying...", gameObject);
					}

					Pool.ArchiveObject(gameObject);

				}

				if (CorrespondingDot == null) {

					RCData = Physics2D.Raycast(transform.position, transform.up, 5, EnemyLayer);



					try {

						if (RCData.collider.GetComponent<DotManager>().CorrespondingDot == null) {

							RCData.collider.GetComponent<DotManager>().CorrespondingDot = gameObject;

							CorrespondingDot = RCData.collider.gameObject;

						}

					} catch {



					}

				} else {

					if (transform.position.y > CorrespondingDot.transform.position.y) {

						if (CorrespondingDot.GetComponent<DotManager>().ToneID == ToneID) {

							CorrespondingDot.GetComponent<DotManager>().Shatter(false);

							if (DebugLogs)
								Debug.Log ("Object Is Shattering");
							
							Pool.ArchiveObject (gameObject);

						} else {

							try {

								CorrespondingDot.GetComponent<DotManager>().CorrespondingDot = null;

							} catch {

								string Excuse = "unknown";

								if (!CorrespondingDot)
									Excuse = "Corresponding dot does not exist";
								else if (!CorrespondingDot.GetComponent<DotManager> ())
									Excuse = "Corresponding dot does not have a DotManager component";

								Debug.LogError ("Corresponding dot reset failed. Reason: " + Excuse, CorrespondingDot);
							
							}

							Retaliate ();

						}

					}

				}

			} else {

				transform.position = new Vector2(transform.position.x, transform.position.y - ((ModAccent/5) * Time.deltaTime));

				if (transform.position.y <= Player.position.y) {

					Player.GetComponent<CharControl>().Die();

				}

				if (transform.position.y < -(Screen.height)/2) {

					if (DebugLogs)
						Debug.Log ("Fell out of screen (Bottom)");
					Pool.ArchiveObject(gameObject);

				}

			}

			if (Player.GetComponent<CharControl>().Dead) {

				Shatter(true);

			}

		}
	
	}

	#if UNITY_EDITOR
	public void OnDrawGizmos () {

		if (CorrespondingDot)
			Gizmos.DrawIcon (transform.position + (Vector3.right * 20), "hasPair-1.png", false);
		else 
			Gizmos.DrawIcon (transform.position + (Vector3.right * 20), "hasPair-0.png", false);

		if (isFromPool)
			Gizmos.DrawIcon (transform.position + (Vector3.right * 50), "isPoolProduct-1.png", false);
		else
			Gizmos.DrawIcon (transform.position + (Vector3.right * 50), "isPoolProduct-0.png", false);

		if (!Bullet)
			Gizmos.DrawIcon (transform.position + (Vector3.right * 80), "isHostile-1.png", false);
		else
			Gizmos.DrawIcon (transform.position + (Vector3.right * 80), "isHostile-0.png", false);

	}
	#endif

	public void Shatter (bool EndGame) {

		GameObject FXInst = Pool.ReInstance (ShatterFX);

		FXInst.transform.SetParent(transform.parent);

		FXInst.transform.position = transform.position;

		FXInst.GetComponentInChildren<SpriteRenderer>().color = GetComponent<SpriteRenderer>().color;

		//FXInst.GetComponent<RectTransform>().localScale = Vector3.one;

		if (!EndGame) {
				
			//AudioSource.PlayClipAtPoint(Boom, Vector2.zero);

			FXInst.GetComponent<AudioSource>().clip = Boom;
			float p = ID;
			FXInst.GetComponent<AudioSource>().pitch = p/10 + 0.9f;
			FXInst.GetComponent<AudioSource>().Play();

			Manager.Score ++;

		}

		if (DebugLogs)
			Debug.Log ("Object Is Shattering (From inside \'DotManager.Shatter()\')");
		Pool.ArchiveObject (gameObject);

	}

	public void Retaliate () {

		Bullet = false;

		Accent = CorrespondingDot.GetComponent<DotManager>().Accent;

		gameObject.layer = 9;

		transform.position = new Vector2 (transform.position.x, CorrespondingDot.transform.position.y - (20));

		CorrespondingDot = null;

		Manager.Score --;

		AudioSource.PlayClipAtPoint(Bom, Vector2.zero);

		GetComponent<AudioSource>().clip = Bom;
		GetComponent<AudioSource>().Play();

	}

	public void ResetDot () {

		gameObject.layer = 5;
		
		ID = 0;
		Bullet = false;
		Active = false;
		isFromPool = true;
		NextDot = null;
		CorrespondingDot = null;

		ModSpeed = 0;
		ModAccent = 0;
		Accent = 0;
		RCData = new RaycastHit2D ();

		Debug.Log ("Reset values for dot with instance ID " + this.GetInstanceID (), this);

	}

}
