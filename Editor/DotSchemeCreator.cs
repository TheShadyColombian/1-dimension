﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



public class DotSchemeCreator {

	[MenuItem ("Assets/Create/Dot Scheme")]
	public static void CreateAsset () {
		CustomAssetUtility.CreateAsset<DotScheme> ();
	}

}