#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("vbfzsLy9t7qnury9oPO8tfOmoLbb+NXS1tbU0dLFzbunp6Og6fz8pPOQkuNR0vHj3tXa+VWbVSTe0tLSGsqhJo7dBqyMSCH20GmGXJ6O3iKxv7bzoKeyvbeyobfzp7ahvqDzsvXj99XQhtfYwM6So6O/tvOQtqGn1ePc1dCGzsDS0izX1uPQ0tIs487m4eLn4+DlicTe4Obj4ePq4eLn420noEg9AbfcGKqc5wtx7SqrLLgbzFZQVshK7pTkIXpIk13/B2JDwQup41HSpePd1dCGztzS0izX19DR0uDlieOx4tjj2tXQhtfVwNGGgOLAE7DgpCTp1P+FOAnc8t0JaaDKnGaWrcyfuINFkloXp7HYw1CSVOBZUgrlrBJUhgp0SmrhkSgLBqJNrXKBobKwp7qwtvOgp7Kntr62vaeg/eNm6X4n3N3TQdhi8sX9pwbv3gixxXsPrfHmGfYGCtwFuAdx9/DCJHJ/7vW081nguSTeURwNOHD8KoC5iLfX1cDRhoDiwOPC1dCG19nA2ZKjo6ySe0sqAhm1T/e4wgNwaDfI+RDMZMhuQJH3wfkU3M5lnk+NsBuYU8RYyloNKpi/JtR48ePRO8vtK4PaAPOyvbfzsLahp7q1urCyp7q8vfOj1tPQUdLc0+NR0tnRUdLS0zdCetqBtr+6sr2wtvO8vfOnu7qg87C2oePC1dCG19nA2ZKjo7+285q9sP3i3tXa+VWbVSTe0tLW1tPQUdLS049R0tPV2vlVm1UksLfW0uNSIeP51flVm1Uk3tLS1tbT47Hi2OPa1dCGU8f4A7qUR6XaLSe4Xv2TdSSUnqzbjeNR0sLV0IbO89dR0tvjUdLX44p01tqvxJOFws2nAGRY8OiUcAa83E7uIPia+8kbLR1mat0Kjc8FGO7UP67qUFiA8wDrF2JsSZzZuCz4L6e6tbqwsqe287Gq87K9qvOjsqGn/ZN1JJSerNuN48zV0IbO8NfL48XF48fV0IbX0MDekqOjv7bzgby8p6O/tvOBvLyn85CS483E3uPl4+fht+bwxpjGis5gRyQlT00cg2kSi4PV0IbO3dfF18f4A7qUR6XaLSe4Xqe7vKG6p6rixePH1dCG19DA3pKj41HXaONR0HBz0NHS0dHS0ePe1drlSp/+q2Q+X0gPIKRIIaUBpOOcEmLjiz+J1+Ffu2Bczg22oCy0jbZvo7+285C2oae6tbqwsqe6vL3zkqZ4cKJBlICGEnz8kmArKDCjHjVwn//zsLahp7q1urCyp7bzo7y/urCqpKT9sqOjv7b9sLy+/LKjo7+2sLKq87KgoKa+tqDzsrCwtqOnsr2wtvzjUhDV2/jV0tbW1NHR41JlyVJgurW6sLKnury985Kmp7u8obqnquJGTanfd5RYiAfF5OAYF9yeHce6ApoLpUzgx7ZypEca/tHQ0tPScFHSzEIIzZSDONY+japX/jjlcYSfhj+/tvOavbD94vXj99XQhtfYwM6So7Rc22fzJBh///O8o2Xs0uNfZJAc9zE4AmSjDNyWMvQZIr6rPjRmxMTzvLXzp7u286e7tr3zsqOjv7qwslygUrMVyIja/EFhK5ebI7PrTcYmg3lZBgk3LwPa1ORjpqby");
        private static int[] order = new int[] { 43,35,13,19,42,10,29,7,50,49,28,50,57,25,58,46,23,54,56,25,54,59,23,45,58,38,49,47,34,35,32,33,52,45,51,39,45,57,39,54,52,51,43,48,49,48,58,49,48,52,57,57,56,56,58,58,59,59,59,59,60 };
        private static int key = 211;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
