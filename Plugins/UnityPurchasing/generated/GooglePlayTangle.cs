#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("vWWoO15MI59IWC7aTZTaoD/vQBxteaRk1QnjPs8Owjxt5vDkBN87c6kxS5I1oVwneetijgKbEIyIk3m4BfFqjfAMOrAINffDcwiefOl7XoFhSoqt+KsJBKrb9vWVO7y9v1ZVZLMwPjEBszA7M7MwMDGVyLsLmaoy22nnl/uQcTQiup1QBzOV0X3aR5bK473n6SLl8poBSVncvbh0v8NzJAGzMBMBPDc4G7d5t8Y8MDAwNDEyBXl1iSSF63Y9FvHRoFpe3Lj3R18kIfwKf4O2Wnq5OsbVzd4b1Aw2lnw7XuzZdOgmXsjeqqqsp5OVSwg/4i1/aBXMGlRawUks7Vz0xmjjxr4OCvg/xoH73HksTw3Kyq6PAvWP52H3JjeUDu7oTDMyMDEw");
        private static int[] order = new int[] { 12,8,10,10,4,8,8,13,12,13,13,13,12,13,14 };
        private static int key = 49;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
